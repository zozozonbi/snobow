set names utf8;

use snobow;

DROP TABLE IF EXISTS user_info;
DROP TABLE IF EXISTS product_info;
DROP TABLE IF EXISTS purchase_history_info;
DROP TABLE IF EXISTS customer;


create table user_info(
    id int not null primary key auto_increment,
    user_id varchar(16) not null unique,
    password varchar(16) not null,
    first_name varchar(32),
    family_name varchar(32),
    prefecture varchar(32),
    municipal varchar(32),
    address varchar(255),
    roll smallint default 1 not null, /* 役割（0：管理者、1：利用者） */
    logined boolean default 0 not null,
    status smallint default 1 not null, /* ステータス（0：無効、1：有効） */
    registed datetime,
    updated datetime
);

create table product_info(
    id int not null primary key auto_increment,
    product_id varchar(16) not null unique,
    product_name varchar(255) not null,
    product_name_kana varchar(255),
    product_description varchar(128),
    product_detail varchar(255),
    product_image varchar(255),
    product_price decimal(8,0) not null,
    registed datetime,
    updated datetime
);

create table purchase_history_info(
    id int not null primary key auto_increment,
    purchase_number int,
    user_id varchar(16) not null,
    product_id varchar(16) not null,
    to_address_id int default 0,
    purchased boolean default 0 not null, /* 購入フラグ（0：購入前、1：購入済み） */
    deleted boolean default 0 not null, /* 削除フラグ（0：未削除、1：削除済み） */
    registed datetime,
    updated datetime
);

create table to_address_info(
    id int not null primary key auto_increment,
    user_id varchar(16) not null,
    first_name varchar(32),
    family_name varchar(32),
    prefecture varchar(32),
    municipal varchar(32),
    address varchar(255),
    status smallint default 1 not null, /* ステータス（0：無効、1：有効） */
    registed datetime,
    updated datetime
);


INSERT INTO user_info(user_id, password, first_name, family_name, prefecture, municipal, address, registed) VALUES('zozozonbi', 'test', 'テスト', '太郎', '東京都', '港区', '浜松町', NOW());

INSERT INTO to_address_info(user_id, first_name, family_name, prefecture, municipal, address, registed) VALUES('zozozonbi', '苗字test', '名前テスト', '東京都', '港区', '浜松町', NOW());

INSERT INTO product_info(product_id, product_name, product_name_kana, product_description, product_detail, product_image, product_price, registed) VALUES('AAA', 'BURTON TestType1', 'バートン テストタイプ1', 'テスト用バートン1', 'テスト用に作られたバートンのテストモデル1', '<img alt="" src="./img/18_nov_misty-ltd-130.jpg" />', 98000, NOW());
INSERT INTO product_info(product_id, product_name, product_name_kana, product_description, product_detail, product_image, product_price, registed) VALUES('BBB', 'BURTON TestType2', 'バートン テストタイプ2', 'テスト用バートン2', 'テスト用に作られたバートンのテストモデル2', '<img alt="" src="./img/burton_test1.jpg" />', 200000, NOW());
INSERT INTO product_info(product_id, product_name, product_name_kana, product_description, product_detail, product_image, product_price, registed) VALUES('CCC', 'BURTON TestType3', 'バートン テストタイプ3', 'テスト用バートン3', 'テスト用に作られたバートンのテストモデル3', '<img alt="" src="./img/burton_test2.jpg" />', 88800, NOW());
