package jp.zozozonbi.snobow.administrator.cntl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Admin {

    @RequestMapping(value = "/hello")  //URLは、http://localhost:8080/helloとする
    public String hello(Model model) {
        //viewに表示する discription を設定する
        model.addAttribute("discription", "Hello!! SpringBootの世界へ");
        return "hello";   //src/main/resources/templates/hello.html を呼び出す
    }

    @RequestMapping(value = "/ad")  //URLは、http://localhost:8080/snobow/adとする
    public String admin(Model model) {
        //viewに表示する discription を設定する
        model.addAttribute("discription", "Hello!! Adminの世界へ");
        return "admin/admin";   //src/main/resources/templates/admin/admin.html を呼び出す
    }

}
