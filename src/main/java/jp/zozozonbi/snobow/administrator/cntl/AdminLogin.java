package jp.zozozonbi.snobow.administrator.cntl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminLogin {

    @RequestMapping(value = "/ad/login")  //URLは、http://localhost:8080/ad/loginとする
    public String adminLogin(Model model) {
        //viewに表示する discription を設定する
        model.addAttribute("discription", "Hello!! Adminの世界へ");
        return "admin/admin_login";   //src/main/resources/templates/ad/admin_login.html を呼び出す
    }

}
