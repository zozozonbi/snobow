package jp.zozozonbi.snobow.administrator.cntl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminProductList {

    @RequestMapping(value = "/ad/adminProductList")  //URLは、http://localhost:8080/ad/listとする
    public String adminList(Model model) {
        //viewに表示する disvription を設定する
        model.addAttribute("");
        return "admin/admin_product_list";
    }

}
