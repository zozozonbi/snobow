package jp.zozozonbi.snobow.administrator.cntl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProductChangeComplete {

    @RequestMapping(value = "/ad/change_complete")  //URLは、http://localhost:8080/ad/change_completeとする
    public String changeComplete(Model model) {
        //viewに表示する discription を設定する
        model.addAttribute("discription", "Hello!! Adminの世界へ");
        return "admin/product_change_complete";   //src/main/resources/templates/ad/change_complete.html を呼び出す
    }

}
