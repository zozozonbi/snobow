package jp.zozozonbi.snobow.administrator.cntl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProductChangeConfirm {

    @RequestMapping(value = "/ad/change_confirm")  //URLは、http://localhost:8080/ad/change_confirmとする
    public String changeConfirm(Model model) {
        //viewに表示する discription を設定する
        model.addAttribute("discription", "Hello!! Adminの世界へ");
        return "admin/product_change_confirm";   //src/main/resources/templates/ad/change_confirm.html を呼び出す
    }
}
