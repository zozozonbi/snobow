package jp.zozozonbi.snobow.administrator.cntl;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ProductChange {

    @RequestMapping(value = "/ad/change")  //URLは、http://localhost:8080/ad/changeとする
    public String productChange(Model model) {
        //viewに表示する discription を設定する
        model.addAttribute("discription", "Hello!! Adminの世界へ");
        return "admin/product_change";   //src/main/resources/templates/ad/product_change.html を呼び出す
    }

}
