package jp.zozozonbi.snobow.serviceInterface;

import jp.zozozonbi.snobow.entity.UserInfoEntity;

public interface LoginServiceInterface {

	public UserInfoEntity Login(String userId, String password);

}
