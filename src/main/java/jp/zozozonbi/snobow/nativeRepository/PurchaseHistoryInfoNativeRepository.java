package jp.zozozonbi.snobow.nativeRepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.zozozonbi.snobow.nativeEntity.PurchaseHistoryInfoNativeEntity;

@Repository
@Transactional
public interface PurchaseHistoryInfoNativeRepository extends JpaRepository<PurchaseHistoryInfoNativeEntity, Integer> {
/*
	@Query(value = "SELECT pi.id AS id, "
		+ "pi.product_name_kana AS productNameKana, "
		+ "pi.product_name AS productName, "
		+ "pi.product_image AS productImage, "
		+ "pi.product_price AS productPrice "
		+ "FROM product_info pi "
		+ "LEFT JOIN purchase_history_info phi ON pi.product_id = phi.product_id "
		+ "WHERE phi.user_id = ?1 AND phi.purchased = ?2 AND phi.deleted = 0 ",
		nativeQuery = true)*/
	@Query(value = "SELECT phi.id AS id, "
			+ "pi.product_image, "
			+ "pi.product_name_kana, "
			+ "pi.product_name, "
			+ "pi.product_price, "
			+ "phi.purchase_number, "
			+ "phi.product_id, "
			+ "phi.purchased, "
			+ "phi.to_address_id, "
			+ "phi.registed, "
			+ "phi.updated "
/*			+ "phi.purchase_number, "
			+ "phi.user_id, "
			+ "phi.product_id, "
			+ "phi.deleted "*/
			+ "FROM purchase_history_info AS phi "
			+ "LEFT JOIN product_info pi ON pi.product_id = phi.product_id "
			+ "WHERE phi.user_id = ?1 AND phi.purchased = ?2 AND phi.deleted = 0 ",
			nativeQuery = true)
	List<PurchaseHistoryInfoNativeEntity> findByUserIdAndPurchased(String userId, boolean purchased);
	
}
