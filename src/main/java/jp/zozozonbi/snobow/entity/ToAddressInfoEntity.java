package jp.zozozonbi.snobow.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="to_address_info")
@Getter
@Setter
public class ToAddressInfoEntity {

	@Id
	@Column(name="id", columnDefinition="auto_increment")
	private int id;
	
	@Column(name="user_id", columnDefinition="varchar(16)", nullable=false)
	private String userId;
	
	@Column(name="first_name", columnDefinition="varchar(32)")
	private String firstName;
	
	@Column(name="family_name", columnDefinition="varchar(32)")
	private String familyName;
	
	@Column(name="prefecture", columnDefinition="varchar(32)")
	private String prefecture;
	
	@Column(name="municipal", columnDefinition="varchar(32)")
	private String municipal;
	
	@Column(name="address", columnDefinition="varchar(255)")
	private String address;
	
	@Column(name="status", columnDefinition="smallint default 1", nullable=false)
	private short status;
	
	@Column(name="registed", columnDefinition="datetime")
	private Timestamp registed;
	
	@Column(name="updated", columnDefinition="datetime")
	private Timestamp updated;
}
