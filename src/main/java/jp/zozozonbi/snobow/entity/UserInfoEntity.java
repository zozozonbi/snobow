package jp.zozozonbi.snobow.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Table(name = "user_info")
@Entity
@Getter
@Setter
public class UserInfoEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    
    @Column(name="user_id", nullable = false, unique = true, columnDefinition = "varchar(16)")
    private String userId;
    
    @Column(name = "password", nullable = false, columnDefinition = "varchar(16)")
    private String password;
    
    @Column(name = "first_name", columnDefinition = "varchar(32)")
    private String firstName;
    
    @Column(name = "family_name", columnDefinition = "varchar(32)")
    private String familyName;
    
    @Column(name = "prefecture", columnDefinition = "varchar(32)")
    private String prefecture;
    
    @Column(name = "municipal", columnDefinition = "varchar(32)")
    private String municipal;
    
    @Column(name = "address", columnDefinition = "varchar(255)")
    private String address;
    
    @Column(name = "roll", nullable = false, columnDefinition = "smallint default 1")
    private short roll;
    
    @Column(name = "logined", nullable = false, columnDefinition = "boolean default 0")
    private boolean logined;
    
    @Column(name = "status", nullable = false, columnDefinition = "smallint default 1")
    private short status;
    
    @Column(name = "registed", columnDefinition = "datetime")
    private Timestamp registed;
    
    @Column(name = "updated", columnDefinition = "datetime")
    private Timestamp updated;
/*
    public UserInfoEntity(String userId, String password, String firstName, String familyName, String prefecture, String municipal, String address, Timestamp registed, Timestamp updated) {
    	this.userId = userId;
    	this.password = password;
    	this.firstName = firstName;
    	this.familyName = familyName;
    	this.prefecture = prefecture;
    	this.municipal = municipal;
    	this.address = address;
    	this.registed = registed;
    	this.updated = updated;
    }
*/
}
