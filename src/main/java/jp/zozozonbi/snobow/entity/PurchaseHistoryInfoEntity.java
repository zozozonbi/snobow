package jp.zozozonbi.snobow.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "purchase_history_info")
@Getter
@Setter
public class PurchaseHistoryInfoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "purchase_number", columnDefinition = "int")
	private int purchaseNumber;
	
	@Column(name = "user_id", columnDefinition = "varchar(16)", nullable = false)
	private String userId;
	
	@Column(name = "product_id", columnDefinition = "varchar(16)", nullable = false)
	private String productId;
	
	@Column(name="to_address_id", columnDefinition="int default 0")
	private int toAddressId;
	
	@Column(name = "purchased", columnDefinition = "boolean default 0", nullable = false)
	private boolean purchased;

	@Column(name = "deleted", columnDefinition = "boolean default 0", nullable = false)
	private boolean deleted;
	
	@Column(name = "registed", columnDefinition = "datetime")
	private Timestamp registed;
	
	@Column(name = "updated", columnDefinition = "datetime")
	private Timestamp updated;
}
