package jp.zozozonbi.snobow.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "product_info")
@Getter
@Setter
public class ProductInfoEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private int id;
	
	@Column(name = "product_id", columnDefinition = "varchar(16)", nullable = false, unique = true)
	private String productId;

	@Column(name = "product_name", columnDefinition = "varchar(255)", nullable = false)
	private String productName;

	@Column(name = "product_name_kana", columnDefinition = "varchar(255)")
	private String productNameKana;

	@Column(name = "product_description", columnDefinition = "varchar(128)")
	private String productDescription;

	@Column(name = "product_detail", columnDefinition = "varchar(255)")
	private String productDetail;

	@Column(name = "product_image", columnDefinition = "varchar(255)")
	private String productImage;

	@Column(name = "product_price", columnDefinition = "decimal(8,0)", nullable = false)
	private BigDecimal productPrice;

	@Column(name = "registed", columnDefinition = "datetime")
	private Timestamp registed;
	
	@Column(name = "updated", columnDefinition = "datetime")
	private Timestamp updated;
}
