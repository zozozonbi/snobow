package jp.zozozonbi.snobow.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.zozozonbi.snobow.entity.UserInfoEntity;
import jp.zozozonbi.snobow.repository.UserInfoRepository;
import jp.zozozonbi.snobow.serviceInterface.LoginServiceInterface;

@Service
public class LoginService implements LoginServiceInterface {

	@Autowired
	private UserInfoRepository userInfoRepository;

	@Override
	public UserInfoEntity Login(String userId, String password) {
		return userInfoRepository.findByUserIdAndPassword(userId, password);
	}
}
