package jp.zozozonbi.snobow.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import jp.zozozonbi.snobow.entity.CustomerEntity;

public interface CustomerRepository extends CrudRepository<CustomerEntity, Integer> {
    List<CustomerEntity> findByLastName(String lastName);
}
