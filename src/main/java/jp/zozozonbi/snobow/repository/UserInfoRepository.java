package jp.zozozonbi.snobow.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.zozozonbi.snobow.entity.UserInfoEntity;

@Repository
@Transactional
public interface UserInfoRepository extends JpaRepository<UserInfoEntity, Integer> {
    public UserInfoEntity findByUserIdAndPassword(String userId, String password);
}
