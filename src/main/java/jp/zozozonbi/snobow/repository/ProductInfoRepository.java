package jp.zozozonbi.snobow.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jp.zozozonbi.snobow.entity.ProductInfoEntity;

@Repository
public interface ProductInfoRepository extends JpaRepository<ProductInfoEntity, Integer> {
	public ProductInfoEntity findByProductId(String productId);
}
