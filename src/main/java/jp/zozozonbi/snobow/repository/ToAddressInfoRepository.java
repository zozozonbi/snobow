package jp.zozozonbi.snobow.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import jp.zozozonbi.snobow.entity.ToAddressInfoEntity;

@Repository
public interface ToAddressInfoRepository extends JpaRepository<ToAddressInfoEntity, Integer> {
	public List<ToAddressInfoEntity> findByUserIdAndStatus(String userId, short status);
}
