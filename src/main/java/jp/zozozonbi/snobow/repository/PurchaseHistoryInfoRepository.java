package jp.zozozonbi.snobow.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import jp.zozozonbi.snobow.entity.PurchaseHistoryInfoEntity;

@Repository
@Transactional
public interface PurchaseHistoryInfoRepository extends JpaRepository<PurchaseHistoryInfoEntity, Integer> {
	public List<PurchaseHistoryInfoEntity> findByUserId(String userd);
	public void deleteByUserIdAndPurchased(String userId, boolean purchased);
	
	@Query(value="UPDATE "
			+ "purchase_history_info "
			+ "SET "
			+ "purchased = 1, "
			+ "to_address_id = ?1, "
			+ "updated = NOW() "
			+ "WHERE "
			+ "user_id = ?2 "
			+ "AND "
			+ "purchased = 0 ",
			nativeQuery=true)
	public JpaRepository<PurchaseHistoryInfoEntity, Integer> updatePurchaseHistoryToAddress(int addressId, String userId);
}
