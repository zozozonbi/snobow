package jp.zozozonbi.snobow.nativeEntity;

import java.math.BigDecimal;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class PurchaseHistoryInfoNativeEntity {

	@Id
	private int id;
	
	private int purchaseNumber;

	private String productNameKana;

	private String productName;

	private String productImage;

	private BigDecimal productPrice;

	private String productId;
	
	private int toAddressId;
	
	private boolean purchased;
	
	private Timestamp registed;
	
	private Timestamp updated;
/*
	private int purchaseNumber;
	
	private String userId;
	
	private boolean deleted;

	private String product_name;

	@Column(name = "product_image", columnDefinition = "varchar(255)")
	private String product_image;
	
	*/
}
