package jp.zozozonbi.snobow;

import java.sql.Timestamp;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import jp.zozozonbi.snobow.entity.CustomerEntity;
import jp.zozozonbi.snobow.entity.UserInfoEntity;
import jp.zozozonbi.snobow.repository.CustomerRepository;
import jp.zozozonbi.snobow.repository.UserInfoRepository;


@SpringBootApplication
public class SnobowApplication {

//    private static final Logger log = LoggerFactory.getLogger(SnobowApplication.class);
//    private static Date dateTime = new Date();
//    private static Timestamp time = new Timestamp(dateTime.getTime());

    public static void main(String[] args) {
        SpringApplication.run(SnobowApplication.class, args);
    }
/*
    @Bean
    public CommandLineRunner demo(UserInfoRepository repository) {
    	return (args)-> {
    		log.info(time.toString());
    		System.out.println("test" + repository);
    		log.info(repository.toString());
    		repository.save(new UserInfoEntity("user", "test", "zonbi", "taro", "tokyo", "������", "�䒃�m��", time, time));
    	};
    }
*/
/*
    @Bean
    public CommandLineRunner demo(CustomerRepository repository) {
        return (args)-> {
            repository.save(new CustomerEntity("Jack", "Bauer"));
            repository.save(new CustomerEntity("Chloe", "O'Brian"));
            repository.save(new CustomerEntity("Kim", "Bauer"));
            repository.save(new CustomerEntity("David", "Palmer"));
            repository.save(new CustomerEntity("Michelle", "Dessler"));

            log.info("Customers found with findAll():");
            log.info("-------------------------------");
            for (CustomerEntity customer : repository.findAll()) {
                log.info(customer.toString());
            }
            log.info("");


            CustomerEntity customer = repository.findOne(1);
            log.info("Customer found with findOne(1L):");
            log.info("--------------------------------");
            log.info(customer.toString());
            log.info("");


            log.info("Customer found with findByLastName('Bauer'):");
            log.info("--------------------------------");
            for (CustomerEntity bauer : repository.findByLastName("Bauer")) {
                log.info(bauer.toString());
            }
            log.info("");
        };
    }
    */
}
