package jp.zozozonbi.snobow.customer.cntl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jp.zozozonbi.snobow.entity.ProductInfoEntity;
import jp.zozozonbi.snobow.repository.ProductInfoRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
public class ProductDetail {

	@Autowired
	private ProductInfoRepository repository;
	
	@RequestMapping(value="/ProductDetail", method=RequestMethod.GET)
	public String productDetail(Model model, @RequestParam("productId") String productId) {
		String res = "service/error";
		if(productId == null || productId == "") {
			return res;
		}

		ProductInfoEntity productInfo = repository.findByProductId(productId);

		model.addAttribute("productInfo", productInfo);
		
		res = "service/productdetail";
		return res;
	}

}
