package jp.zozozonbi.snobow.customer.cntl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.zozozonbi.snobow.entity.PurchaseHistoryInfoEntity;
import jp.zozozonbi.snobow.entity.ToAddressInfoEntity;
import jp.zozozonbi.snobow.nativeEntity.PurchaseHistoryInfoNativeEntity;
import jp.zozozonbi.snobow.nativeRepository.PurchaseHistoryInfoNativeRepository;
import jp.zozozonbi.snobow.repository.PurchaseHistoryInfoRepository;
import jp.zozozonbi.snobow.repository.ToAddressInfoRepository;
import lombok.Getter;
import lombok.Setter;

@Controller
@Getter
@Setter
public class ToAddressSelect {

	private String res = "service/index";

	@Autowired
	private ToAddressInfoRepository toAddressRepository;
	
	@Autowired
	private PurchaseHistoryInfoNativeRepository NativeRepository;
	
	@Autowired
	private PurchaseHistoryInfoRepository purchaseHistoryInfoRepository;
	
	@Autowired
	private HttpSession session;
	
	@GetMapping("/ToAddressSelect")
	public String toAddressSelect(Model model) {
		if (session.getAttribute("userId") == null) {
			return res;
		}
		String userId = session.getAttribute("userId").toString();
		List<ToAddressInfoEntity> addressList = new ArrayList<>();
		addressList = toAddressRepository.findByUserIdAndStatus(userId, (short)1);
		model.addAttribute("addressList", addressList);
		res = "service/to_address_select";
		return res;
	}
	
	@PostMapping("/ToAddressSelect")
	public String toAddressSelect(Model model, @RequestParam("addressId") String addressId) {
		String userId = session.getAttribute("userId").toString();
		try {
			List<PurchaseHistoryInfoNativeEntity> list = NativeRepository.findByUserIdAndPurchased(userId, false);

			for (int i=0; list.size() > i; i++) {
				PurchaseHistoryInfoEntity entity = new PurchaseHistoryInfoEntity();
				entity.setId(list.get(i).getId());
				entity.setPurchaseNumber(list.get(i).getPurchaseNumber());
				entity.setUserId(userId);
				entity.setProductId(list.get(i).getProductId());
				entity.setToAddressId(Integer.parseInt(addressId));
				entity.setPurchased(false); // �w������ꍇtrue
				entity.setDeleted(false);
				entity.setRegisted(list.get(i).getRegisted());
				entity.setUpdated(new Timestamp(new Date().getTime()));

				purchaseHistoryInfoRepository.save(entity);
			}

			//res = "service/paymethodselect";
			res = "service/purchase_confirm";
		} catch(Exception e) {
			e.printStackTrace();
			res = "service/error";
		}
		return res;
	}

}
