package jp.zozozonbi.snobow.customer.cntl;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.zozozonbi.snobow.entity.UserInfoEntity;
import jp.zozozonbi.snobow.serviceInterface.LoginServiceInterface;
import lombok.EqualsAndHashCode;

@Controller
@RequestMapping("/")
@EqualsAndHashCode(callSuper=false)
public class CustomerCntl {

	@Autowired
	HttpSession session;

	@Autowired
	private LoginServiceInterface loginService;

	@RequestMapping("/")
	public String indexCntl(Model model) {
		return "service/index";
	}

	@GetMapping("/Login")
	public String loginCntl(
			RedirectAttributes redirectAttributes, 
			Model model, 
			@RequestParam(name="userId", required=false) String userId, 
			@RequestParam(name="password", required=false) String password, 
			@RequestParam(name="fromCart", required=false) String fromCart, 
			@RequestParam(name="cartType", required=false) String cartType, 
			@RequestParam(name="productId", required=false) String productId) {

		String res = "service/login";

		if (fromCart != null) {
        	model.addAttribute("fromCart", fromCart);
        }
        if (cartType != null) {
        	model.addAttribute("cartType", cartType);
        }
        if (productId != null) {
        	model.addAttribute("productId", productId);
        }

        if (session.getAttribute("userId") != null) {
        	res = "redirect:/";
        }else if (userId != null && password != null) {
        	UserInfoEntity userInfoEntity = loginService.Login(userId, password);
        	if (userInfoEntity != null) {
        		session.setAttribute("userId", userInfoEntity.getUserId());
        		if (fromCart != null && fromCart.equals("fromCart")) {
        			redirectAttributes.addFlashAttribute("cartType", cartType);
                	redirectAttributes.addFlashAttribute("productId", productId);
                	res = "forward:/Cart";
        		} else {
        			res = "service/index";
        		}
        	}
        }

		return res;

	}

	@PostMapping("/Login")
	public String loginCntl() {
		return "service/login";
	}

}
