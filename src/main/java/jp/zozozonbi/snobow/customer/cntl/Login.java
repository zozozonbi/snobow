/*package jp.zozozonbi.snobow.customer.cntl;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.zozozonbi.snobow.entity.UserInfoEntity;
import jp.zozozonbi.snobow.repository.UserInfoRepository;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@Controller
@EqualsAndHashCode(callSuper=false)
public class Login extends Session { //implements UserInfoRepository{

    @Autowired
    HttpSession session;

    @Autowired
    UserInfoRepository repository;

    @PostMapping(value = "/Login")  //URLは、http://localhost:8080/snobow/loginとする
    public String index(RedirectAttributes redirectAttributes, Model model, @RequestParam("userId") String userId, @RequestParam("password") String password, @RequestParam(name="fromCart", required=false) String fromCart, @RequestParam(name="cartType", required=false) String cartType, @RequestParam(name="productId", required=false) String productId) {
        String res = "service/login";
        if (fromCart != null) {
        	model.addAttribute("fromCart", fromCart);
        }
        if (cartType != null) {
        	model.addAttribute("cartType", cartType);
        }
        if (productId != null) {
        	model.addAttribute("productId", productId);
        }

        //if (session.containsKey("userId")) {
        if (session.getAttribute("userId") != null) {
            model.addAttribute("description", "Hello!! Adminの世界へ");
            res = "redirect:/";
        } else if(userId != null && password != null) {
            UserInfoEntity userInfoEntity = repository.findByUserIdAndPassword(userId, password);
            if (userInfoEntity != null) {
                session.setAttribute("userId", userInfoEntity.getUserId());
                if (fromCart != null && fromCart.equals("fromCart")) {
                	redirectAttributes.addFlashAttribute("cartType", cartType);
                	redirectAttributes.addFlashAttribute("productId", productId);
                	return "forward:/Cart";
                }
                res = "service/index";
            }
        }

        return res;   //src/main/resources/templates/service/index.html を呼び出す
    }

    @GetMapping(value = "/Login")
    public String index() {
    	return "service/login";
    }

}
*/