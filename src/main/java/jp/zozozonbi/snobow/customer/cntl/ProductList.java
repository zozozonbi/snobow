package jp.zozozonbi.snobow.customer.cntl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.ServerProperties.Session;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.zozozonbi.snobow.entity.ProductInfoEntity;
import jp.zozozonbi.snobow.repository.ProductInfoRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
public class ProductList extends Session {
	
	@Autowired
	private ProductInfoRepository Repository;
	
	@Autowired
	private HttpSession session;

	private List<ProductInfoEntity> productInfoList = new ArrayList<ProductInfoEntity>();

	@RequestMapping(value="/ProductList", method=RequestMethod.GET)
	public String productList(Model model) {
		String res = "service/productlist";
		productInfoList = Repository.findAll();
		//session.setAttribute("productlist", productInfoList);
		model.addAttribute("productInfoList", productInfoList);
		return res;
		
	}
}
