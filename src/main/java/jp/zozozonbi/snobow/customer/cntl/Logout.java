package jp.zozozonbi.snobow.customer.cntl;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Logout {

    @Autowired
    HttpSession session;

    @RequestMapping(value = "/Logout")
    public String logout() {
        String res = "";

        session.invalidate();
        res = "redirect:/";

        return res;
    }

}
