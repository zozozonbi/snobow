package jp.zozozonbi.snobow.customer.cntl;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import jp.zozozonbi.snobow.entity.PurchaseHistoryInfoEntity;
import jp.zozozonbi.snobow.nativeEntity.PurchaseHistoryInfoNativeEntity;
import jp.zozozonbi.snobow.nativeRepository.PurchaseHistoryInfoNativeRepository;
import jp.zozozonbi.snobow.repository.PurchaseHistoryInfoRepository;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Controller
public class Cart {

	@Autowired
	private PurchaseHistoryInfoNativeRepository NativeRepository;

	@Autowired
	private PurchaseHistoryInfoRepository Repository;

	@Autowired
	HttpSession session;

	private String userId;

	private List<PurchaseHistoryInfoNativeEntity> cartList;

	@PostMapping(value = "/Cart")
	public String postCart(Model model, @RequestParam("cartType") String cartType, @RequestParam(name="productId", required=false) String productId) {
		if(session.getAttribute("userId") == null) {
			model.addAttribute("fromCart", "fromCart");
			model.addAttribute("cartType", cartType);
			model.addAttribute("productId", productId);
			return "service/login";
		} else {
			userId = session.getAttribute("userId").toString();
		}

		String res = "service/cart";
		if(cartType != null && cartType.equals("delete")) {

			List<PurchaseHistoryInfoEntity> entityList = Repository.findByUserId(userId);
			int i = 0;
			int j = 0;
			while(entityList.size() > i) {
				System.out.println("�����O�F" + entityList.get(i).getPurchaseNumber());
				i++;
			}
			Repository.deleteByUserIdAndPurchased(userId, false);
			List<PurchaseHistoryInfoEntity> entityList2 = Repository.findByUserId(userId);
			while(entityList2.size() > j) {
				System.out.println("������F" + entityList2.get(j).getPurchaseNumber());
				j++;
			}
			cartList = NativeRepository.findByUserIdAndPurchased(userId, false);
			model.addAttribute("cartList", cartList);
		} else if(cartType != null && cartType.equals("insert")) {
			System.out.println("INSERT CART START");
			System.out.println("���ݎ���" + new Timestamp(new Date().getTime()));
			PurchaseHistoryInfoEntity entity = new PurchaseHistoryInfoEntity();
			entity.setPurchaseNumber(111);
			entity.setUserId(userId);
			entity.setProductId(productId);
			entity.setPurchased(false);
			entity.setDeleted(false);
			entity.setRegisted(new Timestamp(new Date().getTime()));
			Repository.save(entity);
			cartList = NativeRepository.findByUserIdAndPurchased(userId, false);
			model.addAttribute("cartList", cartList);
			System.out.println("INSERT CART END");
		} else {
			res = "service/error";
		}
		return res;
	}

	@GetMapping(value = "/Cart")
	public String getCart(Model model) {
		String res = "service/login";
		String userId;
		if (session.getAttribute("userId") != null) {
			userId = session.getAttribute("userId").toString();
			cartList = NativeRepository.findByUserIdAndPurchased(userId, false);
			model.addAttribute("cartList", cartList);
			res = "service/cart";
		}
		return res;
	}
}
